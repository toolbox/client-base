module colab.sdk {
    var D = document;
    var W = window;
    var $ = document.querySelectorAll;

    class EventDispatcher {
        private callbacks: { [id: string]: ((event: any) => void)[] };
        on(event: string, callback: (event: any) => void): void {
            this.callbacks[event].push(callback);
        }

        protected dispatch(event: string, data: any) {
            for (var i in this.callbacks[event]) {
                this.callbacks[event][i](data);
            }
        }
    }

    //State class to detect changes
    class State extends EventDispatcher {
        //require get/set access in order to properly 
        private data: any;
        private oldData: any;
        
        private updateRate: number;
        private updateRef: number;


        constructor(d: any) {
            super();
            this.data = d;
            this.oldData = d;
        }

        bindTo(selection: string | DOMSelection) {
            if (typeof selection === "string") {
                selection = new DOMSelection(<string>selection);
            }
        }

        set(d: any): void {
            this.data = d;
            if (this.data !== this.oldData) {
                super.dispatch("change", this);
            }
            this.oldData = d;
        }

        get(): void {
            return this.data;
        }

        value(): void {
            return this.data;
        }

        


    }

    class DOMSelection extends EventDispatcher {
        private selection: Element[];
        private selectors: string[];
        private view: View;
        private state: State;

        constructor(sel: string) {
            super();
            //convert to array
            this.add(sel);
        }

        add(sel: string | NodeListOf<Element> | Element): void {
            try {
                if (typeof sel === "string") {
                    this.selection.push(Array.prototype.slice.call($(sel)));
                    this.selectors.push(sel);
                } else if (typeof sel === "object" && "nodeType" in sel) {
                    this.selection.push(<Element>sel); //must be Element
                } else {
                    this.selection.push(Array.prototype.slice.call(<NodeListOf<Element>>sel));
                }
            } catch (e) {
                console.error("Can't add to DOMSel, not acceptable input");
            }
        }

        update(): void {
            for (var i in this.selectors) {
                var newRoots = $(this.selectors[i]);
                for (var j in newRoots) {
                    if (this.selection.indexOf(newRoots[j]) < 0) {
                        this.selection.push(newRoots[j]);
                    }
                }
            }
        }

        bindView(v: View): void {
            this.view = v;
            if (this.state) {
                this.render();
            }
        }

        bindState(s: State): void {
            this.state = s;
            this.state.on('change', this.render.bind(this));
            if (this.view) {
                this.render();
            }
        }

        render(): void {
            for (var i in this.selection) {
                this.selection[i].innerHTML = this.view(this.state.value());
            }
        }
    }

    interface View {
        (context: any): string
    };
}

