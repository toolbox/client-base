var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var colab;
(function (colab) {
    var sdk;
    (function (sdk) {
        var D = document;
        var W = window;
        var $ = document.querySelectorAll;
        var EventDispatcher = (function () {
            function EventDispatcher() {
            }
            EventDispatcher.prototype.on = function (event, callback) {
                this.callbacks[event].push(callback);
            };
            EventDispatcher.prototype.dispatch = function (event, data) {
                for (var i in this.callbacks[event]) {
                    this.callbacks[event][i](data);
                }
            };
            return EventDispatcher;
        }());
        //State class to detect changes
        var State = (function (_super) {
            __extends(State, _super);
            function State(d) {
                _super.call(this);
                this.data = d;
                this.oldData = d;
            }
            State.prototype.bindTo = function (selection) {
                if (typeof selection === "string") {
                    selection = new DOMSelection(selection);
                }
            };
            State.prototype.set = function (d) {
                this.data = d;
                if (this.data !== this.oldData) {
                    _super.prototype.dispatch.call(this, "change", this);
                }
                this.oldData = d;
            };
            State.prototype.get = function () {
                return this.data;
            };
            State.prototype.value = function () {
                return this.data;
            };
            return State;
        }(EventDispatcher));
        var DOMSelection = (function (_super) {
            __extends(DOMSelection, _super);
            function DOMSelection(sel) {
                _super.call(this);
                //convert to array
                this.add(sel);
            }
            DOMSelection.prototype.add = function (sel) {
                try {
                    if (typeof sel === "string") {
                        this.selection.push(Array.prototype.slice.call($(sel)));
                        this.selectors.push(sel);
                    }
                    else if (typeof sel === "object" && "nodeType" in sel) {
                        this.selection.push(sel); //must be Element
                    }
                    else {
                        this.selection.push(Array.prototype.slice.call(sel));
                    }
                }
                catch (e) {
                    console.error("Can't add to DOMSel, not acceptable input");
                }
            };
            DOMSelection.prototype.update = function () {
                for (var i in this.selectors) {
                    var newRoots = $(this.selectors[i]);
                    for (var j in newRoots) {
                        if (this.selection.indexOf(newRoots[j]) < 0) {
                            this.selection.push(newRoots[j]);
                        }
                    }
                }
            };
            DOMSelection.prototype.bindView = function (v) {
                this.view = v;
                if (this.state) {
                    this.render();
                }
            };
            DOMSelection.prototype.bindState = function (s) {
                this.state = s;
                this.state.on('change', this.render.bind(this));
                if (this.view) {
                    this.render();
                }
            };
            DOMSelection.prototype.render = function () {
                for (var i in this.selection) {
                    this.selection[i].innerHTML = this.view(this.state.value());
                }
            };
            return DOMSelection;
        }(EventDispatcher));
        ;
    })(sdk = colab.sdk || (colab.sdk = {}));
})(colab || (colab = {}));
