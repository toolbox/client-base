/* global Views */
if (typeof 'Views' === 'undefined') {
    var Views: View[] = [];
}

module Client {
    "use strict";
    //Executing code begins here
    //---------------------------------
    var roots = [];//setup global roots store

    //---------------------------------
    //Executing code ends here
    
    
    //functions below
    //---------------------------------
    
    /* Initalizes state and ensures required variables exist
     *
     *
     */
    function initialize() {
        //ensure all views have root stores
        for (var i in Views) {
            if (typeof Views[i].roots === 'undefined') {
                Views[i].roots = [];
            }
        }
        
        //render all html-classes that match view names
        for (var className in Views) {
            bindViewToDOM(Views[className], '.' + className);
        }

    }
    
    /* Binds a selected view to a selected set of DOM elements
     * @param view
     * @param selection
     */
    export function bindViewToDOM(view: View, selection: string) {
        if (!view) { console.warn('Warning: no view has been passed to bindViewToDOM, binding null/undefined view to DOM.'); }
        var newRoots = document.querySelectorAll(selection);
        for (var i in newRoots) { newRoots['view'] = view; }
        
        //add to view's roots, and global roots
        view.roots.push.apply(view.roots, newRoots);
        roots.push.apply(roots, newRoots);
    }

    /* Binds a selected data object to the DOM
     * @param data
     * @param updateFreq
     */
    export function bindDataToDOM(data: any, selection: string) {
        if (!data) { console.warn('Warning: no data has been passed to bindDataToDOM, binding null/undefined data to DOM. '); }
        var newRoots = document.querySelectorAll(selection);
        //link the roots to data and add roots to global list
        data['__roots'] = newRoots;
        roots.push.apply(roots,newRoots);
        for (var i in newRoots) {
            
        }

    }

    export function checkForChanges() {
        for (var i in roots) {
            
        } 
    }
}



interface View {
    roots: Array<Element>
}
