/* global Views */

function bindViewToClass(viewName, className, data) {
    var els = document.getElementsByClassName(className);
    var view = Views[viewName];
    var UUIDs = getUUIDs(data);
    if (UUIDs.length === 0) {
        data['UUID'] = Symbol('Autogen');
        setInterval(function(oldData,view,className) {
            if (data !== oldData) {//probably won't work. need to check if two equals signs is correct
                updateView(view,className);//I think two equals signs is actually good but we need to treat the >2 updates case here. two works, >2 doesn't yet due to binding of oldData
            }
        }.bind(null,data,view,className),1500);
    }
    for (var i in els) {
        els[i].innerHTML = view(data);
        for (var j in UUIDs) {
            els[i].addEventListener("updated" + UUIDs[j], updateView.bind(null, view, className));
        }
    }
    //Placing updateView in this scope gives it access to the data object
    function updateView(view, className, event) {
        var els = document.getElementsByClassName(className);
        for (var i in els) {
            els[i].innerHTML = view(data);
        }
    }
}

function getUUIDs(obj) {
    var UUIDs = [];
    for (var i in obj) {
        if (i === 'UUID') { UUIDs.push(obj[i]); }
        if (obj[i] !== null && typeof (obj[i]) === "object") {
            getUUIDs(obj[i]);
        }
    }
    return UUIDs;
}